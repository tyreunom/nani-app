Nani?
=====

“Nani?” is an offline Japanese dictionary for Android. It helps you find and
understand words without internet access, by downloading dictionary sources.

Naze?
-----

I am a Japanese learner, but never found the right app for this task. My goal
here is to be able to access a Japanese dictionary in different contexts: when
I read, when I want to find a word I don't know in Japanese, when I want to
learn more about a kanji, etc.

This app allows you to download different kinds of dictionaries that enable
some of these functionalities.

License
-------

Nani? is licensed under the GPL 3+ license. It also uses data from third
parties, with different licenses.


| Source name | Provider | License | Description |
| ----------- | -------- | ------- | ----------- |
| [JMdict](https://www.edrdg.org/jmdict/edict_doc.html)      | ERDRG    | CC-BY-SA 3.0 | Provides the main search function |


Contributing
------------

You can contribute in multiple ways:

### Translations

There are two things that need translation in this repo: the app description for app stores, and the app
itself. To translate the description, you will have to download the file corresponding to your language
in `fastlane/metadata/android` and keep it up to date with the source at `fastlane/metadata/android/en-US`.
If there is no directory for your language, copy the files from `en-US` and translate them.

For translating the app, you will have to download `app/src/main/res/values/strings.xml` as the source
and the corresponding file in your language. For instance `app/src/main/res/values-fr/strings.xml` for
French. If the directory doesn't exist for your language, simply copy the English file and translate it.

In any case, once you have translated something, you can submit the translation here by opening an
issue, submitting a merge request or simply by email, whatever is more convenient for you, dear
translator :)

### Report a bug, request improvements

Well, you can use the [Issues](https://framagit.org/nani-project/nani-app/issues) button on the left :)

### Improve dictionary generation, add a new source, ...

This is the right repo to do so!