package eu.lepiller.nani;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.moji4j.MojiConverter;
import com.moji4j.MojiDetector;

import java.util.ArrayList;

import eu.lepiller.nani.dictionary.DictionaryException;
import eu.lepiller.nani.dictionary.DictionaryFactory;
import eu.lepiller.nani.dictionary.IncompatibleFormatException;
import eu.lepiller.nani.dictionary.NoDictionaryException;
import eu.lepiller.nani.result.Result;
import se.fekete.furiganatextview.furiganaview.FuriganaTextView;

public class MainActivity extends AppCompatActivity implements OnTaskCompleted<SearchResult> {
    private LinearLayout result_view;
    private Button search_button;
    private TextView feedback_text;
    private EditText search_form;
    private static ArrayList<Result> savedResults;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        search_button = findViewById(R.id.search_button);
        result_view = findViewById(R.id.results_view);
        feedback_text = findViewById(R.id.feedback);
        search_form = findViewById(R.id.search_form);

        search_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = search_form.getText().toString();
                if (text.isEmpty()) {
                    Snackbar.make(findViewById(R.id.search_form), getString(R.string.no_search), Snackbar.LENGTH_LONG).show();
                    return;
                }

                result_view.removeAllViews();
                search_button.setEnabled(false);
                feedback_text.setText(R.string.feedback_progress);

                DictionaryFactory.prepare(getApplicationContext());
                new SearchTask(MainActivity.this).execute(text);
            }
        });

        if(savedResults != null) {
            showResults(savedResults);
        }
    }

    private static class SearchTask extends AsyncTask<String, Integer, SearchResult> {
        OnTaskCompleted<SearchResult> callback;
        SearchTask(OnTaskCompleted<SearchResult> callback) {
            this.callback = callback;
        }

        @Override
        protected SearchResult doInBackground(String... sInput) {
            String text = sInput[0];

            ArrayList<Result> searchResult;
            try {
                searchResult = DictionaryFactory.search(text);
            } catch (DictionaryException e) {
                return new SearchResult(e);
            }


            if(searchResult == null || searchResult.size() == 0) {
                MojiConverter converter = new MojiConverter();
                try {
                    searchResult = DictionaryFactory.search(converter.convertRomajiToHiragana(text));
                    searchResult.addAll(DictionaryFactory.search(converter.convertRomajiToKatakana(text)));
                } catch (DictionaryException e) {
                    return new SearchResult(e);
                }

                return new SearchResult(searchResult, text, true);
            } else {
                return new SearchResult(searchResult, text, false);
            }
        }

        @Override
        protected void onPostExecute(SearchResult r) {
            callback.onTaskCompleted(r);
        }
    }

    @Override
    public void onTaskCompleted(SearchResult r) {
        search_button.setEnabled(true);
        if(r.isException()) {
            DictionaryException e = r.getException();
            if (e instanceof NoDictionaryException) {
                Snackbar.make(findViewById(R.id.search_form), getString(R.string.no_dic),
                        Snackbar.LENGTH_LONG).show();
            } else if(e instanceof IncompatibleFormatException) {
                Snackbar.make(findViewById(R.id.search_form), String.format(getString(R.string.incompatible_format),
                        ((IncompatibleFormatException) e).getName()), Snackbar.LENGTH_LONG).show();
            }
            return;
        }

        feedback_text.setText("");

        ArrayList<Result> searchResult = r.getResults();
        MojiDetector detector = new MojiDetector();
        String text = r.getText();
        if(searchResult != null && searchResult.size()>0 && !r.isConverted() && detector.hasLatin(text)) {
            MojiConverter converter = new MojiConverter();
            final String moji = converter.convertRomajiToHiragana(r.getText());
            feedback_text.setText(String.format(getString(R.string.feedback_didyoumean), moji));

            feedback_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    search_form.setText(moji);
                    feedback_text.setOnClickListener(null);
                    search_button.callOnClick();
                }
            });
        }
        savedResults = searchResult;
        showResults(searchResult);
    }

    void showResults(ArrayList<Result> searchResult) {
        if(searchResult.size() == 0) {
            feedback_text.setText(R.string.feedback_no_result);
            return;
        }

        int num = 0;
        for(Result result: searchResult) {
            num++;
            if (num > 10)
                break;
            View child_result = getLayoutInflater().inflate(R.layout.layout_result, result_view, false);

            FuriganaTextView kanji_view = child_result.findViewById(R.id.kanji_view);
            LinearLayout senses_view = child_result.findViewById(R.id.sense_view);
            TextView additional_info = child_result.findViewById(R.id.additional_info_view);

            // Populate the data into the template view using the data object
            kanji_view.setFuriganaText(result.getKanjiFurigana());

            StringBuilder additional = new StringBuilder();
            boolean separator = false;
            for (String s : result.getAlternatives()) {
                if (separator)
                    additional.append(getResources().getString(R.string.sense_separator));
                else
                    separator = true;
                additional.append(s);
            }
            additional_info.setText(String.format(getResources().getString(R.string.sense_alternatives), additional.toString()));

            senses_view.removeAllViews();

            int sense_pos = 1;
            for (Result.Sense sense : result.getSenses()) {
                View child = getLayoutInflater().inflate(R.layout.layout_sense, senses_view, false);
                TextView id_view = child.findViewById(R.id.id_view);
                TextView sense_view = child.findViewById(R.id.definition_view);

                id_view.setText(String.format(getResources().getString(R.string.sense_number), sense_pos));

                StringBuilder sb = new StringBuilder();
                boolean separator1 = false;
                for (String s : sense.getGlosses()) {
                    if (separator1)
                        sb.append(getResources().getString(R.string.sense_separator));
                    else
                        separator1 = true;
                    sb.append(s);
                }
                sense_view.setText(sb.toString());

                senses_view.addView(child);
                sense_pos++;
            }

            result_view.addView(child_result);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_dictionaries) {
            Intent intent = new Intent(this, DictionaryActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_about) {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
