package eu.lepiller.nani;

import java.util.ArrayList;

import eu.lepiller.nani.dictionary.DictionaryException;
import eu.lepiller.nani.result.Result;

public class SearchResult {
    private ArrayList<Result> results;
    private DictionaryException exception;
    private boolean converted;
    private String text;

    SearchResult(ArrayList<Result> results, String text, boolean converted) {
        this.results = results;
        this.converted = converted;
        this.text = text;
    }

    SearchResult(DictionaryException e) {
        exception = e;
    }

    boolean isException() {
        return exception != null;
    }

    DictionaryException getException() {
        return exception;
    }

    ArrayList<Result> getResults() {
        return results;
    }

    boolean isConverted() {
        return converted;
    }

    String getText() {
        return text;
    }
}
