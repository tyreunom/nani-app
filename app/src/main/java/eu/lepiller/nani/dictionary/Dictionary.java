package eu.lepiller.nani.dictionary;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.graphics.drawable.VectorDrawableCompat;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

abstract public class Dictionary {
    private String name;
    private String description, fullDescription;
    File file;

    Dictionary(String n, String descr, String fullDescr, File cacheDir) {
        name = n;
        description = descr;
        fullDescription = fullDescr;
        this.file = new File(cacheDir, "/dico/" + name);
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
    public String getFullDescription() {
        return fullDescription;
    }

    protected File getFile() {
        return file;
    }

    abstract public int getSize();

    abstract public boolean isDownloaded();

    abstract public int size();

    // Used for downloads: tells what siwe we currently have downloaded
    abstract int currentSize();

    public abstract Map<URL, File> getDownloads();

    public Drawable getDrawable(Context context) {
        Drawable drawable;
        int drawableResId = getDrawableId();
        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            drawable = context.getResources().getDrawable(drawableResId, context.getTheme());
        } else {
            drawable = VectorDrawableCompat.create(context.getResources(), drawableResId, context.getTheme());
        }
        return drawable;
    }

    abstract int getDrawableId();

    abstract public void remove();
}
