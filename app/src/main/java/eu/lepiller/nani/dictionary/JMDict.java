package eu.lepiller.nani.dictionary;

import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import eu.lepiller.nani.R;
import eu.lepiller.nani.result.Result;

public class JMDict extends Dictionary {
    interface Huffman {
    }

    static class HuffmanTree implements Huffman {
        Huffman left, right;
        HuffmanTree(Huffman left, Huffman right) {
            this.left = left;
            this.right = right;
        }
    }
    static class HuffmanValue implements Huffman {
        String character;
        HuffmanValue(String character) {
            this.character = character;
        }
    }

    final private static String TAG = "JMDICT";
    private String mUrl;
    private Huffman kanjiHuffman, readingHuffman, meaningHuffman;

    JMDict(String name, String description, String fullDescription, File cacheDir, String url) {
        super(name, description, fullDescription, cacheDir);
        mUrl = url;
    }

    @Override
    public boolean isDownloaded() {
        File f = getFile();
        return f.exists();
    }

    @Override
    public int size() {
        return 1;
    }

    @Override
    int currentSize() {
        return 0;
    }

    @Override
    public Map<URL, File> getDownloads() {
        HashMap<URL, File> result = new HashMap<>();
        try {
            result.put(new URL(mUrl), getFile());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    int getDrawableId() {
        return R.drawable.ic_nani_edrdg;
    }

    @Override
    public void remove() {
        File file = getFile();
        file.delete();
        kanjiHuffman = null;
        readingHuffman = null;
        meaningHuffman = null;
    }

    public int getSize() {
        if(!isDownloaded())
            return 0;

        return (int)(getFile().length() / 1000000);
    }

    private String getString(RandomAccessFile file) throws IOException {
        byte b;
        ArrayList<Byte> bs = new ArrayList<>();
        while((b = file.readByte()) != 0) {
            bs.add(b);
        }
        byte[] str = new byte[bs.size()];
        for(int j=0; j<bs.size(); j++) {
            str[j] = bs.get(j);
        }
        return new String(str, "UTF-8");
    }

    private ArrayList<String> getStringList(RandomAccessFile file) throws IOException {
        ArrayList<String> results = new ArrayList<>();
        int number = file.readShort();
        for(int i=0; i<number; i++) {
            results.add(getString(file));
        }
        return results;
    }

    private String getHuffmanString(RandomAccessFile file, Huffman huffman) throws IOException {
        StringBuilder b = new StringBuilder();
        ArrayList<Boolean> bits = new ArrayList<>();
        String c = null;
        Huffman h = huffman;
        while(c == null || !c.isEmpty()) {
            if(h instanceof HuffmanValue) {
                c = ((HuffmanValue) h).character;
                Log.d(TAG, "Huffman read: " + c);
                b.append(c);
                h = huffman;
            } else if(h instanceof HuffmanTree) {
                if(bits.isEmpty()) {
                    byte by = file.readByte();
                    Log.d(TAG, "Read byte for huffman: " + by);
                    for(int i = 7; i>-1; i--) {
                        bits.add((by&(1<<i))!=0);
                    }
                    Log.d(TAG, "Read byte for huffman: " + bits);
                }

                Boolean bo = bits.get(0);
                bits.remove(0);
                h = bo? ((HuffmanTree) h).right: ((HuffmanTree) h).left;
            }
        }

        return b.toString();
    }

    private void logHuffman(Huffman h, ArrayList<Boolean> addr) {
        if (h instanceof HuffmanValue) {
            Log.d(TAG, "HUFF: " + ((HuffmanValue) h).character + " -> " + addr.toString());
        } else if(h instanceof HuffmanTree) {
            ArrayList<Boolean> addr_l = new ArrayList<>(addr);
            addr_l.add(false);
            ArrayList<Boolean> addr_r = new ArrayList<>(addr);
            addr_r.add(true);
            logHuffman(((HuffmanTree) h).left, addr_l);
            logHuffman(((HuffmanTree) h).right, addr_r);
        }
    }

    private ArrayList<String> getHuffmanStringList(RandomAccessFile file, Huffman huffman) throws IOException {
        ArrayList<String> results = new ArrayList<>();
        int number = file.readShort();
        Log.d(TAG, "huffmanStrings: " + number);
        for(int i=0; i<number; i++) {
            results.add(getHuffmanString(file, huffman));
        }
        return results;
    }

    private ArrayList<Integer> getIntList(RandomAccessFile file) throws IOException {
        ArrayList<Integer> results = new ArrayList<>();
        int number = file.readShort();
        for(int i=0; i<number; i++) {
            results.add(Integer.valueOf(file.readByte()));
        }
        return results;
    }

    private Result getValue(RandomAccessFile file, long pos) throws IOException {
        file.seek(pos);
        Log.d(TAG, "Getting value at " + pos);
        ArrayList<String> kanjis = getHuffmanStringList(file, kanjiHuffman);

        Log.d(TAG, "Getting readings");
        ArrayList<Result.Reading> readings = new ArrayList<>();
        int reading_number = file.readShort();
        Log.d(TAG, reading_number + " readings.");
        for(int i=0; i<reading_number; i++) {
            ArrayList<String> reading_kanjis = getStringList(file);
            Log.d(TAG, "kanjis: " + reading_kanjis);
            ArrayList<String> reading_infos = getStringList(file);
            Log.d(TAG, "infos: " + reading_kanjis);
            ArrayList<String> reading_readings = getHuffmanStringList(file, readingHuffman);
            Result.Reading r = new Result.Reading(reading_kanjis, reading_infos, reading_readings);
            readings.add(r);
        }

        ArrayList<Result.Sense> senses = new ArrayList<>();
        int meaning_number = file.readShort();
        Log.d(TAG, meaning_number + " meanings.");
        for(int i=0; i<meaning_number; i++) {
            ArrayList<String> sense_references = getStringList(file);
            ArrayList<String> sense_limits = getStringList(file);
            ArrayList<String> sense_infos = getStringList(file);
            ArrayList<Result.Source> sense_sources = new ArrayList<>();
            int source_number = file.readShort();
            for(int j=0; j<source_number; j++) {
                ArrayList<String> source_content = getStringList(file);
                boolean source_wasei = file.read() != 0;
                String source_type = getString(file);
                String source_language = getString(file);
                sense_sources.add(new Result.Source(source_content, source_wasei, source_type, source_language));
            }
            ArrayList<Integer> sense_tags = getIntList(file);
            ArrayList<String> sense_glosses = getHuffmanStringList(file, meaningHuffman);
            String sense_language = getString(file);
            senses.add(new Result.Sense(sense_references, sense_limits, sense_infos, sense_sources,
                    sense_tags, sense_glosses, sense_language));
        }
        return new Result(kanjis, readings, senses);
    }

    private ArrayList<Integer> getValues(RandomAccessFile file, long triePos) throws IOException {
        file.seek(triePos);
        Log.d(TAG, "Getting values");
        int valuesLength = file.readShort();
        ArrayList<Integer> results = new ArrayList<>();

        Log.d(TAG, "Number of values: " + valuesLength);
        for(int i=0; i<valuesLength; i++) {
            results.add(file.readInt());
        }

        int transitionLength = file.readByte();
        Log.d(TAG, "Number of transitions: " + transitionLength);
        int[] others = new int[transitionLength];
        for(int i=0; i<transitionLength; i++) {
            file.skipBytes(1);
            others[i] = file.readInt();
        }

        for(int i=0; i<transitionLength; i++) {
            results.addAll(getValues(file, others[i]));
        }

        Log.d(TAG, "result size: " + results.size());
        return results;
    }

    private ArrayList<Integer> searchTrie(RandomAccessFile file, long triePos, byte[] txt) throws IOException {
        Log.d(TAG, "searchTrie: " + triePos);
        Log.d(TAG, "Remaining transitions: " + new String(txt, "UTF-8"));
        file.seek(triePos);
        Log.d(TAG, "pointer: " + file.getFilePointer());
        if(txt.length == 0) {
            return getValues(file, triePos);
        } else {
            int valuesLength = file.readShort();
            Log.d(TAG, "number of values: " + valuesLength);
            file.skipBytes(valuesLength * 4);

            int transitionLength = file.readByte();
            Log.d(TAG, "number of transitions: " + transitionLength);

            for(int i = 0; i < transitionLength; i++) {
                byte letter = file.readByte();
                Log.d(TAG, "Possible transition " + letter + "; Expected transition: " + txt[0]);
                if(letter == txt[0]) {
                    Log.d(TAG, "Taking transition "+letter);
                    byte[] ntxt = new byte[txt.length-1];
                    System.arraycopy(txt, 1, ntxt, 0, txt.length-1);
                    return searchTrie(file, file.readInt(), ntxt);
                } else {
                    file.skipBytes(4);
                }
            }

            return null;
        }
    }

    private Huffman loadHuffman(RandomAccessFile file) throws IOException {
        byte b = file.readByte();
        if(b == 1) {
            Huffman left = loadHuffman(file);
            Huffman right = loadHuffman(file);

            return new HuffmanTree(left, right);
        } else if (b == 0) {
            Log.d(TAG, "Skipping byte " + file.readByte());
            return new HuffmanValue("");
        } else {
            ArrayList<Byte> bs = new ArrayList<>();
            bs.add(b);
            while((b = file.readByte()) != 0) {
                bs.add(b);
            }
            byte[] array = new byte[bs.size()];
            for(int i=0; i<bs.size(); i++) {
                array[i] = bs.get(i);
            }
            return new HuffmanValue(new String(array, "UTF-8"));
        }
    }

    ArrayList<Result> search(String text) throws IncompatibleFormatException {
        if (isDownloaded()) {
            try {
                RandomAccessFile file = new RandomAccessFile(getFile(), "r");
                byte[] header = new byte[14];
                int l = file.read(header);
                if (l != header.length)
                    return null;

                // Check file format version
                if(!Arrays.equals(header, "NANI_JMDICT001".getBytes()))
                    throw new IncompatibleFormatException(getName());

                byte[] search = text.getBytes();

                long kanjiTriePos = file.readInt();
                long readingTriePos = file.readInt();
                long meaningTriePos = file.readInt();

                Log.d(TAG, "Search in: " + getFile());
                Log.d(TAG, "kanji: " + kanjiTriePos);
                Log.d(TAG, "reading: " + readingTriePos);
                Log.d(TAG, "meaning: " + meaningTriePos);

                kanjiHuffman = loadHuffman(file);
                readingHuffman = loadHuffman(file);
                meaningHuffman = loadHuffman(file);

                logHuffman(readingHuffman, new ArrayList<Boolean>());

                // Search in Japanese, by kanji and reading
                ArrayList<Integer> results = searchTrie(file, kanjiTriePos, search);
                ArrayList<Integer> readingResults = searchTrie(file, readingTriePos, search);
                if(results != null && readingResults != null)
                    results.addAll(readingResults);
                else if (results == null)
                    results = readingResults;

                // Search in other language, by meaning
                if(results == null || results.isEmpty())
                    results = searchTrie(file, meaningTriePos, search);

                if(results == null || results.isEmpty())
                    return null;

                ArrayList<Result> r = new ArrayList<>();
                ArrayList<Integer> uniqResults = new ArrayList<>();
                for(Integer i: results) {
                    if(!uniqResults.contains(i))
                        uniqResults.add(i);
                }

                int[] uniqResultsArray = new int[uniqResults.size()];
                for(int i=0; i<uniqResults.size(); i++) {
                    uniqResultsArray[i] = uniqResults.get(i);
                }
                Arrays.sort(uniqResultsArray);

                Log.d(TAG, uniqResults.toString());

                int num = 0;
                for(int i: uniqResultsArray) {
                    if(num > 10)
                        break;
                    num++;
                    r.add(getValue(file, i));
                }
                return r;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
