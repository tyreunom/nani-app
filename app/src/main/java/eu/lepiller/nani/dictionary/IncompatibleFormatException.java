package eu.lepiller.nani.dictionary;

public class IncompatibleFormatException extends DictionaryException {
    String name;

    public IncompatibleFormatException(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
