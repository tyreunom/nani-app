package eu.lepiller.nani.dictionary;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import eu.lepiller.nani.R;

public class DictionariesAdapter extends ArrayAdapter<Dictionary> {
    Context context;
    public DictionariesAdapter(Context context, ArrayList<Dictionary> dictionaries) {
        super(context, 0, dictionaries);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Dictionary dictionary = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_dictionary, parent, false);
        }

        if (dictionary == null) {
            return convertView;
        }

        // Lookup view for data population
        ImageView icon_view = convertView.findViewById(R.id.icon_view);
        TextView name_view = convertView.findViewById(R.id.name_view);
        TextView description_view = convertView.findViewById(R.id.additional_info_view);

        // Populate the data into the template view using the data object
        Drawable icon = dictionary.getDrawable(context);
        if(icon != null) {
            icon = icon.getConstantState().newDrawable().mutate();
            if(!dictionary.isDownloaded())
                icon.setAlpha(64);
            else
                icon.setAlpha(255);
            icon_view.setImageDrawable(icon);
        }

        name_view.setText(dictionary.getName());
        description_view.setText(dictionary.getDescription());

        // Return the completed view to render on screen
        return convertView;
    }
}
