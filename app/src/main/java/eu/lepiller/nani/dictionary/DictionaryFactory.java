package eu.lepiller.nani.dictionary;

import android.content.Context;

import java.util.ArrayList;

import eu.lepiller.nani.R;
import eu.lepiller.nani.result.Result;

public class DictionaryFactory {
    private static DictionaryFactory instance;
    private static ArrayList<Dictionary> dictionaries;

    private DictionaryFactory(Context context) {
        dictionaries = new ArrayList<>();
        dictionaries.add(new JMDict("JMdict_e",
                context.getString(R.string.dico_jmdict_e),
                context.getString(R.string.dico_jmdict_long),
                context.getCacheDir(),
                "https://nani.lepiller.eu/dicos/JMdict_e.nani"));
        dictionaries.add(new JMDict("JMdict_dut",
                context.getString(R.string.dico_jmdict_dut),
                context.getString(R.string.dico_jmdict_long),
                context.getCacheDir(),
                "https://nani.lepiller.eu/dicos/JMdict_dut.nani"));
        dictionaries.add(new JMDict("JMdict_fre",
                context.getString(R.string.dico_jmdict_fre),
                context.getString(R.string.dico_jmdict_long),
                context.getCacheDir(),
                "https://nani.lepiller.eu/dicos/JMdict_fre.nani"));
        dictionaries.add(new JMDict("JMdict_ger",
                context.getString(R.string.dico_jmdict_ger),
                context.getString(R.string.dico_jmdict_long),
                context.getCacheDir(),
                "https://nani.lepiller.eu/dicos/JMdict_ger.nani"));
        dictionaries.add(new JMDict("JMdict_hun",
                context.getString(R.string.dico_jmdict_hun),
                context.getString(R.string.dico_jmdict_long),
                context.getCacheDir(),
                "https://nani.lepiller.eu/dicos/JMdict_hun.nani"));
        dictionaries.add(new JMDict("JMdict_rus",
                context.getString(R.string.dico_jmdict_rus),
                context.getString(R.string.dico_jmdict_long),
                context.getCacheDir(),
                "https://nani.lepiller.eu/dicos/JMdict_rus.nani"));
        dictionaries.add(new JMDict("JMdict_slv",
                context.getString(R.string.dico_jmdict_slv),
                context.getString(R.string.dico_jmdict_long),
                context.getCacheDir(),
                "https://nani.lepiller.eu/dicos/JMdict_slv.nani"));
        dictionaries.add(new JMDict("JMdict_spa",
                context.getString(R.string.dico_jmdict_spa),
                context.getString(R.string.dico_jmdict_long),
                context.getCacheDir(),
                "https://nani.lepiller.eu/dicos/JMdict_spa.nani"));
        dictionaries.add(new JMDict("JMdict_swe",
                context.getString(R.string.dico_jmdict_swe),
                context.getString(R.string.dico_jmdict_long),
                context.getCacheDir(),
                "https://nani.lepiller.eu/dicos/JMdict_swe.nani"));
    }

    public static void prepare(Context context) {
        if(instance == null)
            instance = new DictionaryFactory(context);
    }

    public static ArrayList<Result> search(String text) throws DictionaryException {
        if(instance == null)
            throw new NoDictionaryException();

        int available = 0;
        ArrayList<Result> results = new ArrayList<>();
        for(Dictionary d: dictionaries) {
            if (d instanceof JMDict && d.isDownloaded()) {
                available++;
                ArrayList<Result> dr = ((JMDict) d).search(text);
                if(dr != null)
                    results.addAll(dr);
            }
        }

        if(available == 0) {
            throw new NoDictionaryException();
        }
        return results;
    }

    public static Dictionary get(int position) {
        return dictionaries.get(position);
    }

    public static ArrayList<Dictionary> getDictionnaries(Context context) {
        if(instance == null)
            instance = new DictionaryFactory(context);

        return dictionaries;
    }
}
