package eu.lepiller.nani.result;

import android.util.Log;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS;

public class Result {
    public static class Source {
        private ArrayList<String> content;
        private boolean wasei;
        private String type, language;

        public Source(ArrayList<String> content, boolean wasei, String type, String language) {
            this.content = content;
            this.wasei = wasei;
            this.type = type;
            this.language = language;
        }
    }

    public static class Sense {
        private ArrayList<String> references, limits, infos, glosses;
        private ArrayList<Integer> tags;
        private String language;
        private ArrayList<Source> sources;

        public Sense(ArrayList<String> references, ArrayList<String> limits, ArrayList<String> infos,
                     ArrayList<Source> sources, ArrayList<Integer> tags, ArrayList<String> glosses,
                     String language) {
            this.references = references;
            this.limits = limits;
            this.infos = infos;
            this.sources = sources;
            this.tags = tags;
            this.glosses = glosses;
            this.language = language;
        }

        public ArrayList<String> getGlosses() {
            return glosses;
        }
    }

    public static class Reading {
        private ArrayList<String> kanjis, infos, readings;

        public Reading(ArrayList<String> kanjis, ArrayList<String> infos, ArrayList<String> readings) {
            this.kanjis = kanjis;
            this.infos = infos;
            this.readings = readings;
        }
    }

    private ArrayList<String> kanjis;
    private ArrayList<Reading> readings;
    private ArrayList<Sense> senses;

    public Result(ArrayList<String> kanjis, ArrayList<Reading> readings, ArrayList<Sense> senses) {
        this.kanjis = kanjis;
        this.readings = readings;
        this.senses = senses;
    }

    public String getKanji() {
        String k = getReading();
        if(kanjis.size() > 0)
            k = kanjis.get(0);
        return k;
    }

    public ArrayList<String> getAlternatives() {
        return kanjis;
    }

    public ArrayList<Sense> getSenses() {
        return senses;
    }

    public ArrayList<Reading> getReadings() {
        return readings;
    }

    public String getReading() {
        String reading = "";
        if(readings.size() > 0) {
            ArrayList<String> rs = readings.get(0).readings;
            if(rs.size() > 0)
                reading = rs.get(0);
        }
        return reading;
    }

    public String getKanjiFurigana() {
        String txt = getKanji();
        String reading = getReading();
        Log.d("RESULT", "reading: " + reading);

        // split the text into kanji / not kanji portions
        ArrayList<String> portions = new ArrayList<>();

        StringBuilder current = new StringBuilder();
        Character.UnicodeBlock b = CJK_UNIFIED_IDEOGRAPHS;

        for(int i=0; i<txt.length(); i++) {
            Character.UnicodeBlock b2 = Character.UnicodeBlock.of(txt.charAt(i));
            if(b == b2) {
                current.append(txt.charAt(i));
            } else {
                String s = current.toString();
                if(!s.isEmpty())
                    portions.add(s);
                current = new StringBuilder();
                current.append(txt.charAt(i));
            }

            b = b2;
        }
        String str = current.toString();
        if(!str.isEmpty())
            portions.add(str);

        // Create a regexp to match kanji places
        current = new StringBuilder();
        current.append("^");
        for(String s: portions) {
            if(Character.UnicodeBlock.of(s.charAt(0)) == CJK_UNIFIED_IDEOGRAPHS) {
                current.append("(.*)");
            } else {
                current.append(s);
            }
        }
        current.append("$");

        Log.d("RESULT", "regex: " + current.toString());

        Pattern p = Pattern.compile(current.toString());
        Matcher m = p.matcher(reading);

        if(!m.matches()) {
            Log.d("RESULT", "Finaly: " + txt);
            return txt;
        }

        // We have a match!

        Log.d("RESULT", "matched");

        current = new StringBuilder();
        int group = 1;
        for(String s: portions) {
            if(Character.UnicodeBlock.of(s.charAt(0)) == CJK_UNIFIED_IDEOGRAPHS) {
                current.append("<ruby>");
                current.append(s);
                current.append("<rt>");
                current.append(m.group(group));
                current.append("</rt>");
                current.append("</ruby>");
                group++;
            } else {
                current.append(s);
            }
        }
        Log.d("RESULT", "Finaly: " + current.toString());
        return current.toString();
    }
}
