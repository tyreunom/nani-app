package eu.lepiller.nani;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import eu.lepiller.nani.dictionary.DictionariesAdapter;
import eu.lepiller.nani.dictionary.DictionaryFactory;
import eu.lepiller.nani.dictionary.Dictionary;

public class DictionaryActivity extends AppCompatActivity {
    static final int DICO_REQUEST = 1;
    DictionariesAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dictionary);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.title_activity_dictionary);
        setSupportActionBar(toolbar);

        final ListView list_view = findViewById(R.id.dictionary_view);
        final ArrayList<Dictionary> dictionaries = DictionaryFactory.getDictionnaries(getApplicationContext());
        adapter = new DictionariesAdapter(getApplicationContext(), dictionaries);
        list_view.setAdapter(adapter);

        list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(DictionaryActivity.this, DictionaryDownloadActivity.class);
                intent.putExtra("dico", position);
                startActivityForResult(intent, DICO_REQUEST);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == DICO_REQUEST) {
            adapter.notifyDataSetChanged();
        }
    }

}
