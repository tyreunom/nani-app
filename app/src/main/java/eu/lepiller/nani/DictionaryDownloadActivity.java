package eu.lepiller.nani;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import eu.lepiller.nani.dictionary.DictionariesAdapter;
import eu.lepiller.nani.dictionary.Dictionary;
import eu.lepiller.nani.dictionary.DictionaryFactory;

public class DictionaryDownloadActivity extends AppCompatActivity {
    final static String TAG = "DWN";

    ProgressBar download_bar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dictionary_download);

        int position = getIntent().getExtras().getInt("dico");

        final Dictionary d = DictionaryFactory.get(position);
        setResult(DictionaryActivity.DICO_REQUEST);
        updateLayout(d);
    }

    private void updateLayout(final Dictionary d) {
        TextView name_view = findViewById(R.id.name_view);
        name_view.setText(d.getName());

        TextView description_view = findViewById(R.id.additional_info_view);
        description_view.setText(d.getDescription());
        TextView full_description_view = findViewById(R.id.extended_info_view);
        full_description_view.setText(d.getFullDescription());

        download_bar = findViewById(R.id.download_progress);
        download_bar.setProgress(0);

        ImageView icon_view = findViewById(R.id.icon_view);
        Drawable icon = d.getDrawable(getApplicationContext());
        if (icon != null) {
            icon_view.setImageDrawable(icon);
        }

        Drawable drawable;
        int drawableResId = d.isDownloaded() ? R.drawable.ic_nani_refresh : R.drawable.ic_nani_download;
        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            drawable = getResources().getDrawable(drawableResId, getTheme());
        } else {
            drawable = VectorDrawableCompat.create(getResources(), drawableResId, getTheme());
        }
        final ImageView download_button = findViewById(R.id.download_button);
        download_button.setImageDrawable(drawable);
        download_button.setEnabled(true);

        drawableResId = R.drawable.ic_nani_trash;
        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            drawable = getResources().getDrawable(drawableResId, getTheme());
        } else {
            drawable = VectorDrawableCompat.create(getResources(), drawableResId, getTheme());
        }
        ImageView trash_button = findViewById(R.id.remove_button);
        trash_button.setImageDrawable(drawable);

        LinearLayout remove_layout = findViewById(R.id.remove_layout);
        remove_layout.setVisibility(d.isDownloaded()? View.VISIBLE: View.INVISIBLE);

        TextView size_view = findViewById(R.id.size_view);
        size_view.setText(String.format(getResources().getString(R.string.dictionary_size), d.getSize()));

        download_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                download_button.setEnabled(false);
                final DownloadTask downloadTask = new DownloadTask(DictionaryDownloadActivity.this);
                downloadTask.execute(d);
            }
        });

        trash_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.remove();
                updateLayout(d);
            }
        });
    }

    private class DownloadTask extends AsyncTask<Dictionary, Integer, String> {
        private Dictionary d;
        private Context context;

        public DownloadTask(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(Dictionary... sDicos) {
            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            d = sDicos[0];

            Log.d(TAG, "Start downloading");

            for (Map.Entry<URL, File> e : d.getDownloads().entrySet()) {
                try {
                    URL url = e.getKey();
                    Log.d(TAG, "URL: " + url.toString());
                    connection = (HttpURLConnection) url.openConnection();
                    connection.connect();

                    // expect HTTP 200 OK, so we don't mistakenly save error report
                    // instead of the file
                    if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                        Log.e(TAG, "Server returned HTTP " + connection.getResponseCode()
                                + " " + connection.getResponseMessage());
                        return "Server returned HTTP " + connection.getResponseCode()
                                + " " + connection.getResponseMessage();
                    }

                    // this will be useful to display download percentage
                    // might be -1: server did not report the length
                    int fileLength = connection.getContentLength();

                    // download the file
                    input = connection.getInputStream();
                    File file = e.getValue();
                    file.getParentFile().mkdirs();
                    output = new FileOutputStream(file);

                    byte data[] = new byte[4096];
                    long total = 0;
                    int count;
                    while ((count = input.read(data)) != -1) {
                        // allow canceling with back button
                        if (isCancelled()) {
                            input.close();
                            return null;
                        }
                        total += count;
                        // publishing the progress....
                        if (fileLength > 0) // only if total length is known
                            publishProgress((int) (total * 100 / fileLength));
                        output.write(data, 0, count);
                    }
                } catch (Exception ex) {
                    Log.e(TAG, ex.toString());
                    return ex.toString();
                } finally {
                    try {
                        if (output != null)
                            output.close();
                        if (input != null)
                            input.close();
                    } catch (IOException ignored) {
                    }

                    if (connection != null)
                        connection.disconnect();
                }
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            download_bar.setIndeterminate(true);
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            // if we get here, length is known, now set indeterminate to false
            download_bar.setIndeterminate(false);
            download_bar.setMax(100);
            download_bar.setProgress(progress[0]);
        }


        @Override
        protected void onPostExecute(String result) {
            download_bar.setProgress(100);
            updateLayout(d);
        }
    }
}
